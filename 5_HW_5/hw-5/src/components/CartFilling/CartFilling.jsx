import CardProduct from '../product/Card.jsx'
import { useSelector } from 'react-redux';
import { selectCartArray } from '../../store/selector';
import FormPurchase from '../FormPurchase/FormPurchase';
import React from 'react';

const CartFilling = () => {
  const arrayCart = useSelector(selectCartArray);

    return (
      <>
      <div className='cart-filling'>
        {
           arrayCart.map((product, ind) => {
            return <CardProduct key={ind} cardInfo={product} isInCart={true} />
          })
        }
      </div>
       {!!(arrayCart.length) ? <FormPurchase/> : <p className='title__form'>Товар оформлено!</p> }
       </>
    )
}

export default CartFilling;
