import { useFormik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { selectCartArray } from '../../store/selector';
import { validationSchema } from './validate';
import { setCartProd } from '../../store/action';
import Input from './InputTypes/InputTypes';

const FormPurchase = () => {
    const dispatch = useDispatch();
    const cartArr = useSelector(selectCartArray);
    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phone: '',
            email: ''
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            let sum = 0;
            const resultArr = cartArr.map(({imgUrl, ...info}) => {
                sum += (+info.price);
                return info;
            });
            console.log("%cOrder info", "color:black");
            console.group();
            console.log("%cClient info", "color:white");
            console.table(values);
            console.group();
            console.log("%cBought products", "color:silver");
            console.table(resultArr);
            console.groupEnd();
            console.log('Sum to pay: ', sum);
            dispatch(setCartProd([]));
        }
    })
    return(
        <div className='form__conteiner'>
            <h3 className='title__form'>Оформлення замовлення</h3>
            <form className='form'>
                <Input 
                    {...formik.getFieldProps('firstName')}
                    error={formik.touched.firstName && formik.errors.firstName}
                    errorMessage={formik.errors.firstName}
                    type='text'
                    placeholder='Ім*я'
                    label='Введіть ваше ім*я:'
                    name='firstName' />
                <Input 
                    {...formik.getFieldProps('lastName')}
                    error={formik.touched.firstName && formik.errors.firstName}
                    errorMessage={formik.errors.firstName}
                    type='text'
                    placeholder='Прізвище'
                    label='Введіть ваше прізвище:'
                    name='lastName' />
                <Input 
                    {...formik.getFieldProps('age')}
                    error={formik.touched.age && formik.errors.age}
                    errorMessage={formik.errors.age}
                    type='text'
                    placeholder='Вік'
                    label='Введіть ваш вік:'
                    name='age'/>
                <Input 
                    {...formik.getFieldProps('address')}
                    error={formik.touched.address && formik.errors.address}
                    errorMessage={formik.errors.address}
                    type='text'
                    placeholder='Адреса'
                    label='Введіть поштову адресу:'
                    name='address' />
                <Input 
                    {...formik.getFieldProps('phone')}
                    error={formik.touched.phone && formik.errors.phone}
                    errorMessage={formik.errors.phone}
                    type='text'
                    placeholder='Телефон'
                    label='Введіть ваш номер телефону:'
                    name='phone' />
                <Input
                    {...formik.getFieldProps('email')}
                    error={formik.touched.email && formik.errors.email}
                    errorMessage={formik.errors.email}
                    type='text'
                    placeholder='Електронна пошта'
                    label='Введіть вашу електронну пошту:'
                    name='email' />
                <div className="form-btn">
                    <input type="submit" value="ОФОРМИТИ" />
                </div>
            </form>
        </div>
    )
}

export default FormPurchase;