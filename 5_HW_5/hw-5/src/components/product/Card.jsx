import Button from '../button/Button.jsx';
import Checkbox from '../CheckBox/CheckBox.jsx';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import { selectFavoriteProd } from '../../store/selector';
import { setFavoriteProd, setChosenCard, setDeleteModalRender, setModalRender } from '../../store/action';

const CardProduct = ({ cardInfo, isFavoriteProd, isInCart, isInFavorite }) => {
  const dispatch = useDispatch();
  const [isFavorite, setFavorite] = useState(isFavoriteProd);
  const arrayFavorite = useSelector(selectFavoriteProd);
  const addToFavorites = () => {
    setFavorite(!isFavorite);
    let sameElem = arrayFavorite.find(card => card.productArticle === cardInfo.productArticle);
    let newArr;
    if (arrayFavorite && !sameElem) {
      newArr = [...arrayFavorite,cardInfo];
    } else {
      newArr = arrayFavorite.filter(card => card.productArticle !== cardInfo.productArticle)
    }
    dispatch(setFavoriteProd(newArr));
  }
  const showModal = () => {
    dispatch(setChosenCard(cardInfo));
    dispatch(setModalRender(true));
  }

  const showDeleteModal = () => {
    dispatch(setChosenCard(cardInfo));
    dispatch(setDeleteModalRender(true));
  }
  const { productName, price, imgURL, productArticle, productColor } = cardInfo;
  return (
    <div className='conteiner-card'>
       <div className='conteiner-image'>
         {isInCart && <FontAwesomeIcon className='btn-close' icon={faXmark} onClick={() => showDeleteModal()} />}
         <img src={imgURL} alt="card img" />
       </div>
      {!isInCart ? (
        <div className='card-header' onClick={addToFavorites}>
          <h3 className="card__title">{productName}</h3>
          {isInFavorite ? <Checkbox isFavorite={true} /> : <Checkbox isFavorite={isFavorite} />}
        </div>
      )
        : (
          <div className='card-header'>
            <h3 style={{ cursor: "auto" }} className="card__title">{productName}</h3>
          </div>
        )
      }
      <div className='conteiner__card-desc'>
        <p className="card__desk-product">Product color: {productColor}</p>
        <p className="card__desk-product">Article: {productArticle}</p>
      </div>
      <div className='card-footer'>
        <p className='card-price'>{price} UAH</p>
        {isInCart ? <Button text="Купити" /> : <Button onClick={showModal} text="add to cart" />}

      </div>
    </div>
  )
}

CardProduct.propTypes = {
  cardInfo: PropTypes.object,
};

CardProduct.defaultProps = {
  isFavoriteProd: false,
  deleteCard: null,
  isInCart: false,
  isInFavorite: false
};

export default CardProduct;
