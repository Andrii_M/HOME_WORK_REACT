import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import {selectCartArray, selectFavoriteProd} from '../../store/selector';

const Header = () => {
  const favoriteProd = useSelector(selectFavoriteProd);
  const arrayCart = useSelector(selectCartArray);
  return (
    <header className="conteiner">
      <nav className="navigation">
        <NavLink to="/" style={{ textDecoration: "none" }}>
          <h1 className="conteiner-text">Autotehniks</h1>
        </NavLink>
        <div className="conteiner-icon">
          <NavLink to="/Cart">
            <cart className="conteiner-cart">
              <FontAwesomeIcon icon={faCartShopping} />
              <span className="cart">{arrayCart.length}</span>
            </cart>
          </NavLink>
          <NavLink to="/Favorite">
            <favourite className="conteiner-favourite">
              <FontAwesomeIcon icon={faHeart} />
              <span>{favoriteProd.length}</span>
            </favourite>
          </NavLink>
        </div>
      </nav>
    </header>
  );
}

Header.propTypes = {
  arrayCart: PropTypes.array
};

Header.defaultProps = {
  arrayCart: []
};

// const Header = ({ arrayFavorite, arrayCart }) => {

//   return (
//     <header className="conteiner">
//         <nav className="navigation">
//         <NavLink to="/"
//           style={{ textDecoration: "none" }}>
//           <h1 className="conteiner-text">Autotehniks</h1>
//         </NavLink>
//         <div className="conteiner-icon">
//         <NavLink to="/Cart">
//         <cart className="conteiner-cart">
//           <FontAwesomeIcon icon={faCartShopping} />
//           <span className="cart">{arrayCart.length}</span>
//         </cart>
//         </NavLink>
//         <NavLink to="/Favorite">
//         <favourite className="conteiner-favourite">
//           <FontAwesomeIcon icon={faHeart} />
//           <span>{arrayFavorite.length}</span>
//         </favourite>
//           </NavLink>
//         </div>
//       </nav>
//     </header>
//   );
// }

// Header.propTypes = {
//   arrayFavorite: PropTypes.array,
//   arrayCart: PropTypes.array
// };

// Header.defaultProps = {
//   arrayFavorite: [],
//   arrayCart: []
// };

export default Header;