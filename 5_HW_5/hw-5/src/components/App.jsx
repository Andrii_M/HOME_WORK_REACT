import Button from './button/Button';
import Modal from './modal/Modal';
import ProductWrapper from './ProductWrapper/ProductWrapper'; 
import CartFilling from './CartFilling/CartFilling'; 
import Favourite from './Favourite/Favourite'; 
import Header from './header/Header';
import { useEffect } from 'react';
import * as React from 'react';
import { Routes, Route} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux';
import './App.scss';
import { selectModalRender, selectModalDeleteRender, selectChosenCard, selectProductArray, selectCartArray } from '../store/selector';
import { setCartProd, setChosenCard, setDeleteModalRender, setModalRender } from '../store/action';
import { fetchProducts } from '../store/action'

const App = () => {
  const dispatch = useDispatch();
  const modalRender = useSelector(selectModalRender);
  const modalDeleteRender = useSelector(selectModalDeleteRender);
  const chosenCard = useSelector(selectChosenCard);
  const arrayCart = useSelector(selectCartArray);
  const productArray = useSelector(selectProductArray);
  

  useEffect(() => {
      dispatch(fetchProducts());
  })


  const addToCart = () => {
      let newArr;
      let sameElem = arrayCart.find(card => card.productArticle === chosenCard.productArticle);
      if (!sameElem) {
        newArr = [...arrayCart,chosenCard];
      } else {
        newArr = [...arrayCart];
      }
    dispatch(setCartProd(newArr));
    dispatch(setModalRender(false));
    dispatch(setChosenCard(null));
  }

 
  const closeModal = () => {
    dispatch(setModalRender(false));
    dispatch(setDeleteModalRender(false));
    dispatch(setChosenCard(null));
  }

  const removeCard = () => {
    const newArr = arrayCart.filter(card => card.productArticle !== chosenCard.productArticle);
    dispatch(setCartProd(newArr));
    dispatch(setDeleteModalRender(false));
    dispatch(setChosenCard(null))
  }
 
 
  let modalElement = null;
  let modalDeleteElement = null;
  if (modalRender) {
    modalElement = <Modal header="Повідомлення" text="Ви точно хочете додати товар до вашої корзини?" closeButton={true} onClose={closeModal}>
      <Button text="OК" onClick={addToCart} />
      <Button text="Відмінити" onClick={closeModal} />
    </Modal>;
  }
  if (modalDeleteRender) {
    modalDeleteElement = <Modal header="Повідомлення" text="Ви дійсно хочете видалити продукт із корзини?" closeButton={true} onClose={closeModal}>
      <Button text="Видалити" onClick={removeCard} />
      <Button text="Відмінити" onClick={closeModal} />
    </Modal>;
  }

  return (
    <>
      {modalDeleteElement}
      {modalElement}
      <Header />
      <Routes>
        <Route path="/" element={<ProductWrapper cardsArray={productArray} />} />
        <Route path="Cart" element={<CartFilling />} />
        <Route path="Favorite" element={<Favourite />} />
        <Route path="*" element={<p style={{paddingTop: "150px", textAlign:'center',fontSize:'40px', color: 'white'}}>Помилка 404!</p>}>
        </Route>
      </Routes>
    </>
  );
}

export default App;
