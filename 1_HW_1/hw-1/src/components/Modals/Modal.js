import { Component } from "react";
import { ReactComponent as CloseLogo }  from './cross.svg'
import {  ModalContainer, ModalHeader, ModalClose, ModalFooter, ModalMain, OutsideModal } from './styles'

class Modals extends Component {

    render() {
        const { header, text, closeButton, onClose, children }= this.props;
        const btnClose = closeButton?<ModalClose><CloseLogo onClick={onClose} style={{width: "inherit",height: "inherit"}}/></ModalClose>:null;

        return (
        <>
            <OutsideModal onClick={onClose}/> 
            <ModalContainer className='modal'>
                <ModalHeader className = 'header-text'>
                    <h1 className = 'title-text'>{header}</h1>
                    {btnClose}
                </ModalHeader>
                <ModalMain className = 'main-text'>
                    <p className = 'main-info'>{text}</p>
                </ModalMain>
                <ModalFooter className = 'footer-text'>
                    {children}
                </ModalFooter>
            </ModalContainer>
        </>
      )
    }
}

export default Modals