import { Component } from 'react';

class Buttons extends Component {

    render() {
        const {backgroundColor, text, onClick} = this.props;
        return (<btn className = 'btn-info' onClick={onClick} style={{ backgroundColor: backgroundColor }}> {text} </btn>)
    }
}

export default Buttons;