import React from 'react';
import Buttons from './Buttons/Buttons.js';
import Modal from './Modals/Modal.js'

import './App.scss';

class Application extends React.Component {
    constructor(props) {
      super(props);
      this.state={
        firstModalRender: false,
        secondModalRender: false
      }
      this.showFirstModal = this.showFirstModal.bind(this);
      this.showSecondModal = this.showSecondModal.bind(this);
      this.closeModal = this.closeModal.bind(this);
    }
    showFirstModal(){
      this.setState({
        firstModalRender: true
      })
    }
    showSecondModal(){
      this.setState({
        secondModalRender: true
      })
    }
    closeModal(){
      this.setState({
        firstModalRender: false,
        secondModalRender: false
      })
    }
    render() {
      const buttonFirst = <Buttons backgroundColor="rgb(0 0 0 / 20%)" text="Oк"/>;
      const buttonSecond = <Buttons backgroundColor="rgb(0 0 0 / 20%)" text="Cancel"/>;
      const buttonThird = <Buttons backgroundColor="lightsalmon" text="Докладніше"/>;
      let firsrModalElement = null;
      let secondModalElement = null;
      if (this.state.firstModalRender) {
        firsrModalElement =  <Modal header="Do you want to delete this file?" text="Once you delete this file, it won’t be possible to undo this action. are you sure you want to delete it?" closeButton={true} onClose={this.closeModal}>{buttonFirst}{buttonSecond}</Modal>;
      }
      if (this.state.secondModalRender) {
        secondModalElement =  <Modal header="React Info" text="React — JavaScript-библиотека с открытым исходным кодом для разработки пользовательских интерфейсов. React разрабатывается и поддерживается Facebook, Instagram и сообществом отдельных разработчиков и корпораций." closeButtons={false} onClose={this.closeModal}>{buttonThird}</Modal>;
      }
      return (
      <section className = 'btn-style'>
        <Buttons onClick={this.showFirstModal} backgroundColor="blue" text="Open first modal" className = 'btn-left'/>
        <Buttons onClick={this.showSecondModal} backgroundColor="green" text="Open second modal" className = 'btn-right'/>
        {firsrModalElement}
        {secondModalElement}
      </section>
      );
    };
  }
  
  export default Application;