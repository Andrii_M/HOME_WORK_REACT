import CardProduct from '../product/Card.jsx';
import PropTypes from 'prop-types';
const Favorite = ({ cardsArray, showModal, addToFavorite }) => {
    let cards = null;
    if (Array.isArray(cardsArray)) {
      cards = cardsArray.map((product, ind) => {
        return <CardProduct addToFavoriteFunc={addToFavorite} showModal={showModal} key={ind} cardInfo={product} isFavoriteProd={true} />
      });

    }
    return (
      <div className='favourite'>
        {cards}
      </div>
    )
}

Favorite.propTypes = {
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func
};

Favorite.defaultProps = {
  addToCart: null,
  addToFavorite: null
};

export default Favorite;