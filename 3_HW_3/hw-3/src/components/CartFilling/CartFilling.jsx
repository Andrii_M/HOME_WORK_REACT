import CardProduct from '../product/Card.jsx'
import PropTypes from 'prop-types';
const CartFilling = ({ cardsArray, addToFavorite,showDeleteModal }) => {
    let cards = null;
    if (Array.isArray(cardsArray)) {
      cards = cardsArray.map((product, ind) => {
        return <CardProduct addToFavoriteFunc={addToFavorite}  key={ind} cardInfo={product} isInCart={true} showDeleteModal={showDeleteModal}/>
      });

    }
    return (
      <div className='cart-filling'>
        {cards}
      </div>
    )
}

CartFilling.propTypes = {
  addToCart: PropTypes.func,
  addToFavorite: PropTypes.func
};

CartFilling.defaultProps = {
  addToCart: null,
  addToFavorite: null,
  deleteCard: null
};

export default CartFilling;