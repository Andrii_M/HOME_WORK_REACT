import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as faStarRegular } from '@fortawesome/free-regular-svg-icons';
import PropTypes from 'prop-types';
const Checkbox = ({ isFavorite }) => {

    let iconStar;
    if (isFavorite) {
      iconStar = faStar;
    } else {
      iconStar = faStarRegular;
    }

    return (
      <div className='check-box'>
        <FontAwesomeIcon icon={iconStar} />
      </div>
    );
}

Checkbox.propTypes = {
  isFavorite: PropTypes.bool
};

Checkbox.defaultProps = {
  isFavorite: false
};


export default Checkbox;