import Button from './buttons/buttons';
import Modal from './Modal/Modal';
import ProductWrapper from './ProductWrapper/ProductWrapper';
import Header from './Header/Header';
import React from 'react';
import { Section } from './style.js';
import './App.scss';


class App extends React.Component {
    state = {
      modalRender: false,
      productArray: null
    }
    addToCart = (prod) => {
      let arrayCart = localStorage.getItem("ProdInCart");
      if (arrayCart === null) {
        arrayCart = new Array(0);
      } else {
        arrayCart = JSON.parse(arrayCart);
        arrayCart.push(prod); 
      }
      localStorage.setItem("ProdInCart", JSON.stringify(arrayCart));
      this.setState({
        modalRender: true
      })
    }
    addToFavorites = (prod, isFavorite) => {
      let arrayFavorite = JSON.parse(localStorage.getItem("FavoriteProd"));
      if (isFavorite) {
        arrayFavorite.push(prod)
      } else {
        arrayFavorite = arrayFavorite.filter(card => card.productArticle !== prod.productArticle)
      }
      localStorage.setItem("FavoriteProd", JSON.stringify(arrayFavorite));
      this.setState({
        modalRender: false
      })
    }
    closeModal = () => {
      this.setState({
        modalRender: false
      })
    }
    componentDidMount() {
      const url = process.env.PUBLIC_URL + "/product.json";
      fetch(url)
        .then(res => res.json())
        .then(data => {
          this.setState({
            productArray: data
          })
        })
    }
    render() {
      let arrayFavorite = localStorage.getItem("FavoriteProd");
      let arrayCart = localStorage.getItem("ProdInCart");
      if (arrayFavorite === null) {
        arrayFavorite = new Array(0);
        localStorage.setItem("FavoriteProd", JSON.stringify(arrayFavorite));
      }
      if (arrayCart === null) {
        arrayCart = new Array(0);
        localStorage.setItem("ProdInCart", JSON.stringify(arrayCart));
      }
      const buttonModal = <Button text="OK" onClick={this.closeModal} />;
      const modalButton = <Button text="CANCEL" onClick={this.closeModal} />;
      let modalElement = null;
      if (this.state.modalRender) {
        modalElement = <Modal header="Повідомлення" text="Ви хочете додати товар додано до корзини?" closeButton={false} onClose={this.closeModal}>{buttonModal} {modalButton}</Modal>;
      }
      const { productArray } = this.state;
      return (
        <>
          {modalElement}
          <Header arrayFavorite={JSON.parse(arrayFavorite)} arrayCart={JSON.parse(arrayCart)} />
          <Section>
            <ProductWrapper addToCart={this.addToCart} addToFavorite={this.addToFavorites} cardsArray={productArray} />
          </Section>
        </>
      );
    };
  }
  
  export default App;
  