import { Component } from 'react';
import Button from '../buttons/buttons.jsx';
import Checkbox from '../CheckBox/CheckBox.jsx';
import PropTypes from 'prop-types';


class CardProduct extends Component {
  state = {
    isFavorite: false
  }
  changeFavorites = () => {
    let value = !this.state.isFavorite;
    const { addToFavoriteFunc, cardInfo } = this.props;
    addToFavoriteFunc(cardInfo, value);
    this.setState({
      isFavorite: value
    })
  }
  render() {
    const { cardInfo, addToCartFunc } = this.props;
    const { productName, price, imgURL, productArticle, productColor } = cardInfo;
    return (
      <div className='conteiner-card'>
        <div className='conteiner-image'>
          <img src={imgURL} alt="card img" />
        </div>
        <div className='card-header' isFavorite={this.state.isFavorite} onClick={this.changeFavorites}>
          <h3 className="card__title">{productName}</h3>
          <Checkbox isFavorite={this.state.isFavorite} />
        </div>
        <div className='conteiner__card-desc'>
          <p className="card__desk-product">Color: {productColor}</p>
          <p className="card__desk-product">Article: {productArticle}</p>
        </div>
        <div className='card-footer'>
          <p className='card-price'>{price} UAH</p>
          <Button onClick={() => addToCartFunc(cardInfo)} text="add to cart" backgroundColor="rgb(215, 201, 184)" />
        </div>
      </div>
    )
  };
}

CardProduct.propTypes = {
  cardInfo: PropTypes.object
};

export default CardProduct;
