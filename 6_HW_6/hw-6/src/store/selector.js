export const  selectModalRender = (state) => state.modalRender;
export const  selectModalDeleteRender = (state) => state.modalDeleteRender;
export const  selectChosenCard = (state) => state.chosenCard;
export const  selectProductArray = (state) => state.productArray;
export const  selectCartArray = (state) => state.cartArray;
export const  selectFavoriteProd = (state) => state.favoriteProd;
