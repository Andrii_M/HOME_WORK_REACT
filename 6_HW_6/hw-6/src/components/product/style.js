import styled from "styled-components";
export const Container = styled.div`
.conteiner-image {
    ${prop => prop.rowView?'width: 80px; min-height: 70px;':''};
    img {
      border-radius: ${prop => prop.rowView?'10px':'10px 10px 0 0'};
    }
}

.card {
  ${prop => prop.rowView?'display: flex;align-items: center;justify-content: space-around; padding: 30px 10px; width: 90%; flex-basis: 50px; min-height: 100px;':''};
}

`