import CardProduct from '../product/Card.jsx';
import { useSelector } from 'react-redux';
import {selectFavoriteProd} from '../../store/selector';

import { ViewContext } from '../../context';
import { useContext } from 'react';

const Favourite = () => {
  const { rowView } = useContext(ViewContext);
  const favoriteProd = useSelector(selectFavoriteProd);
  return (
    <div className='favourite' rowView={rowView}>
      {!!(favoriteProd.length)?favoriteProd.map((product, ind) => {
          return <CardProduct key={ind} cardInfo={product} isFavoriteProd={true} isInFavorite={true}/>
        })
        : <p className = 'title__form'>Відсутні улюблені товари</p>
      }
    </div>
  )
}

export default Favourite;