import CardProduct from '../product/Card.jsx'
import { useSelector } from 'react-redux';
import { selectCartArray } from '../../store/selector';
import FormPurchase from '../FormPurchase/FormPurchase';
import React from 'react';

import { ViewContext } from '../../context/index';
import { useContext } from 'react';
 
const CartFilling = () => {
  const arrayCart = useSelector(selectCartArray);
  const { rowView } = useContext(ViewContext);

    return (
      <>
        <div className='cart-filling' rowView={rowView}>
          {
            arrayCart.map((product, ind) => {
              return <CardProduct key={ind} cardInfo={product} isInCart={true} />
            })
          }
        </div>
        {!!(arrayCart.length) ? <FormPurchase/> : <p className='title__form'>Товар відсутній!</p> }
      </>
    )
}

export default CartFilling;
