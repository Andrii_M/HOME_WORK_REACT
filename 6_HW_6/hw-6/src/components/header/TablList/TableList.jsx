import PropTypes from 'prop-types';
import { ViewButtonWrapper, ButtonWrapper } from './style';

const LookButton = ({ children, idComponent, isActive}) => {
    return (
        <ButtonWrapper>
            <ViewButtonWrapper data-testid='test' id={idComponent} active={isActive}>
                { children }
            </ViewButtonWrapper>
        </ButtonWrapper>
    );
}

LookButton.propTypes = {
    isActive: PropTypes.bool
};

LookButton.defaultProps = {
    isActive: false,
    idComponent: ''
};

export default LookButton;