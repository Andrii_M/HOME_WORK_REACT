import styled from "styled-components";

export const ViewButtonWrapper = styled.div`
    font-size: 28px;
    cursor: pointer;
    color: ${prop=>prop.active?'yellowgreen':'blue'};
    display: inline;
    flex-wrap: no-wrap;
    &:hover {
        color: yellowgreen;
    }
`

export const ButtonWrapper = styled.div `
    display: inline;
    margin-right: 10px;
    background-color: white;
    padding: 20px 15px 10px 15px;
    border-radius: 10px;
`