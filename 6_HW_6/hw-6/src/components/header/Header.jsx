import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faTableCells } from '@fortawesome/free-solid-svg-icons';

import { NavLink } from 'react-router-dom';

import { useSelector } from 'react-redux';
import { useContext, useState } from 'react';

import { selectCartArray, selectFavoriteProd } from '../../store/selector';

import { ViewContext } from '../../context';

import LookButton from './TablList/TableList';


const Header = () => {
  const { changeInRowView } = useContext(ViewContext);
  const favoriteProd = useSelector(selectFavoriteProd);
  const arrayCart = useSelector(selectCartArray);
  const [rowActive, setRowActive] = useState(false);
  const [columnActive, setColumnActive] = useState(false);
  
  const handleChangeRow = (e) => {
    const target = e.target.closest('div');
    if (target.id === "row-view") {
      changeInRowView(true);
      setRowActive(true);
      setColumnActive(false);
    } else if (target.id === "column-view") {
      changeInRowView(false);
      setRowActive(false);
      setColumnActive(true);
    }
  }

  return (
    <header className="conteiner">
      <nav className="navigation">
        <NavLink to="/" style={{ textDecoration: "none" }}>
          <h1 className="conteiner-text">Autotehniks</h1>
        </NavLink>
        <div className='conteiner-icon' >
          <LookButton idComponent={"row-view"} isActive={rowActive}>
            <FontAwesomeIcon onClick={handleChangeRow} icon={faBars} />
          </LookButton>
          <LookButton idComponent={"column-view"} isActive={columnActive}>
            <FontAwesomeIcon onClick={handleChangeRow} icon={faTableCells} />
          </LookButton>
        </div>
        <div className="conteiner-icon">
          <NavLink to="/Cart">
            <cart className="conteiner-cart">
              <FontAwesomeIcon icon={faCartShopping} />
              <span className="cart">{arrayCart.length}</span>
            </cart>
          </NavLink>
          <NavLink to="/Favorite">
            <favourite className="conteiner-favourite">
              <FontAwesomeIcon icon={faHeart} />
              <span>{favoriteProd.length}</span>
            </favourite>
          </NavLink>
        </div>
      </nav>
    </header>
  );
}

export default Header;
