import FormPurchase from '../components/FormPurchase/FormPurchase'
import {render, cleanup} from "@testing-library/react";

import {store} from "../store";
import {Provider} from 'react-redux';

afterEach(cleanup);

describe('FormPurchase component', () => {
  it('find input type', () => {
    const {container} = render(<Provider store={store}><FormPurchase/></Provider>);
      const professionInput = container.querySelector('[name="firstName"]').type;
      expect(professionInput).toBe("text")
  })

  it('find button submit', () => {
    const {container} = render(<Provider store={store}><FormPurchase/></Provider>);
      const professionInput = container.querySelector('[value="ОФОРМИТИ"]').type;
      expect(professionInput).toBe("submit")
  })
})
